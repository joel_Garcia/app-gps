import React, { Component } from 'react';
import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import {
  StyleSheet
} from 'react-native';

export default class App extends Component<{}> {

  constructor(props) {
    super(props)
    this.state = {
      coords: [],
      latitude_start: 0.0,
      longitude_start: 0.0
    };

  }

  componentWillMount() {
    return fetch('https://gpssenddata-api.herokuapp.com/api/v1/places')
      .then((response) => response.json())
      .then((responseJson) => {

        let latitude_start = responseJson.start.latitude;
        let longitude_start = responseJson.start.longitude;

        let latitude_end = responseJson.end.latitude;
        let longitude_end = responseJson.end.longitude;

        this.addStartPlaceEndPlace(latitude_start, longitude_start);
        this.getDirections(`${ latitude_start }, ${ longitude_start }`,`${ latitude_end }, ${ longitude_end }`);

    })
    //this.getDirections("20.52639, -97.46278", "18.754, -96.0158")
  }

  addStartPlaceEndPlace(latitude_start, longitude_start) {
    var start = Number(latitude_start);
    var end = Number(longitude_start);
    this.setState({latitude_start: start, longitude_start: end});
  }

  async getDirections(startLoc, destinationLoc) {
      try {

        let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`)
        let respJson = await resp.json();

        let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
        let coords = points.map((point, index) => {
            return  {
                latitude : point[0],
                longitude : point[1]
            }
        })
        this.setState({coords: coords})
        return coords
      } catch(error) {
        alert(error)
        return error
      }
  }

  render() {

    const {latitude_start, longitude_start} = this.state;
    var latitude_start = this.state.latitude_start;
    var longitude_start = this.state.longitude_start;
    console.log(latitude_start);

    return (

      <MapView style={styles.map} initialRegion={{
          latitude: latitude_start, // here I wanna pass the latitude_start variable
          longitude: longitude_start, // here I wanna pass the longitude_start variable
          latitudeDelta: 0.0043,
          longitudeDelta: 0.0034
        }}
      >
        <MapView.Marker
            coordinate={{ latitude: latitude_start, // here I wanna pass the latitude_start variable
            longitude: longitude_start }} // here I wanna pass the longitude_start variable
            title={"Objeto localizado"}
            description={"el ladron esta aqui!"}
        />

        <MapView.Polyline
          coordinates={this.state.coords}
          strokeWidth={10}
          strokeColor="red"/>

      </MapView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
